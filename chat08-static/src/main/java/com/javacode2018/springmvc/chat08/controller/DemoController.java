package com.javacode2018.springmvc.chat08.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 公众号：路人甲java，专注于java干货分享
 * 个人博客：http://itsoku.com/
 * 已推出的系列有：【spring系列】、【java高并发系列】、【MySQL系列】、【MyBatis系列】、【Maven系列】
 * git地址：https://gitee.com/javacode2018
 */
@Controller
public class DemoController {

    @RequestMapping("/demo.do")
    public String demo() {
        return "/WEB-INF/view/demo.jsp";
    }
}
